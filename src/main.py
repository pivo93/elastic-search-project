# external dependencies
from flask import Flask, request, render_template
from elasticsearch import Elasticsearch, helpers as es_hp
import sys

# custom dependencies
import helpers as hp

# the Flask server
app = Flask(__name__)

# initialize elastic search
es = Elasticsearch([
    { 'host': 'elasticsearch' },
])

# constants
ES_INDEX = "movies"
LAST_INDEX_SCRAPED_TXT = "src/last_movie_scraped_index.txt"
INDEX_HTML_FILE = "index.html"
DISPLAY_SEARCH_HTML_FILE = "search-result.html"
DISPLAY_SEARCH_DIRECTORS_HTML_FILE = "search-result-directors.html"
DISPLAY_SEARCH_LGBTQ_HTML_FILE = "search-result-lgbtq.html"

# index route
@app.route("/")
def index():
    return render_template(INDEX_HTML_FILE)

# search World War II movies (since 1980) 
@app.route("/search/ww2")
def search_ww2():
    # query elastic search for the wwII movies
    query = hp.get_es_query_wwII()
    movies = es.search(index=ES_INDEX, body=query)

    # the received movies are nested in movies.hits.hits
    movies = movies['hits']['hits']

    return render_template(DISPLAY_SEARCH_HTML_FILE, json=movies, search="WWII")

# search directors with highest number of action films
@app.route("/search/action_directors")
def search_action_directors():
    # query elastic search for the action movie directors
    query = hp.get_es_query_directors()
    movies = es.search(index=ES_INDEX, body=query)

    # the received aggregations are nested in movies.aggregations.types_count.buckets
    aggregations = movies['aggregations']['types_count']['buckets']

    return render_template(DISPLAY_SEARCH_DIRECTORS_HTML_FILE, json=aggregations, search="Directors with highest number of action films")

# search movies with corrupt politicians in Europe and the US
@app.route("/search/corrupt_politicians")
def search_corrupt_politicians():
    # query elastic search for the corrupt politician movies
    query = hp.get_es_query_corrupt_politicians()
    movies = es.search(index=ES_INDEX, body=query)

    # the received movies are nested in movies.hits.hits
    movies = movies['hits']['hits']

    return render_template(DISPLAY_SEARCH_HTML_FILE, json=movies, search="Movies with corrupt politicians in Europe and the US")

# search LGTBQ movies and also include the number of films per year
@app.route("/search/lgtbq")
def search_lgtbq():
    # query elastic search for the lgbtq movies
    query = hp.get_es_query_lgbtq()
    movies = es.search(index=ES_INDEX, body=query)

    # the aggregations received are nested in movies.aggregations.types_count.buckets
    aggregations = movies['aggregations']['types_count']['buckets']

    # the received movies are nested in movies.hits.hits
    movies = movies['hits']['hits']

    return render_template(DISPLAY_SEARCH_LGBTQ_HTML_FILE, json=movies, aggregations=aggregations, search="LGTBQ (including the number of films per year)" )

# DELETE and POST the elastic search index
@app.route("/elastic/index", methods = [ "POST", "DELETE" ])
def api_elastic_index():
    # create the index in elastic search, ignore status code 400 (index already exists)
    if request.method == "POST":
        print ("Create elasticsearch index '" + ES_INDEX + "... start")
        request_body = hp.get_elasticsearch_index_structure()
        es_create_index_resp = es.indices.create(index=ES_INDEX, ignore=400, body=request_body)    

        # evaluate the elastic search create index response
        if "acknowledged" in es_create_index_resp:
            if es_create_index_resp["acknowledged"] == True:
                print ("Create elasticsearch index '" + es_create_index_resp["index"] + "'... done")

        # catch API error response
        elif "error" in es_create_index_resp:
            print ("ERROR:", es_create_index_resp["error"]["root_cause"])
            print ("TYPE:", es_create_index_resp["error"]["type"])
            return "Elastic Search index creation failed"

        return "Elastic Search index sucessfully created"

    # delete the 'movies' index (if it already exists)
    elif request.method == "DELETE":
        print ("Delete elasticsearch index '" + ES_INDEX + "'... start")
        es.indices.delete(index=ES_INDEX, ignore=[400, 404])
        print ("Delete elasticsearch index '" + ES_INDEX + "... done")

        # when the elastic search index is dropped, also reset the index of the last scraped url
        indexFile = open(LAST_INDEX_SCRAPED_TXT, "w") 
        indexFile.write(str(0))

        # close the file
        indexFile.close()

        return "Elastic Search index sucessfully deleted"

# insert bulk data into the elstic search index
@app.route("/elastic", methods = [ "POST" ])
def api_elastic():
    ##################################################
    # Extract from Excel: 
    ##################################################
    # store all the IMDB Url's of the Excel file
    print ("Extracting Imdb Url's from the excel file... start")
    movie_urls = hp.extract_urls_from_excel()
    print ("Extracting Imdb Url's from the excel file... done")

    ##################################################
    # Web Scraping: 
    ##################################################
    # the index of the url within the excel file to start scraping
    last_index_scraped = 0
    
    try:
        print ("Get last scraped index from TXT file... start")
        # iterating through the movies doesn't necessarily start from index 0 but from the last processed index
        indexFile = open(LAST_INDEX_SCRAPED_TXT, "r") 
        last_index_scraped = int(indexFile.read()) 
        # close the file
        indexFile.close()
        print ("Get last scraped index from TXT file... done")
    except:
        print ("Get last scraped index from TXT file... FAILED")
        return "File containing the last scraped index could not be read / or file input not parseable to int"

    try:
        print ("Web Scraping of Url's... start")
        movie_list = []

        # iterate through all movie url's and scrape them
        while last_index_scraped < len(movie_urls):
            movie_url = movie_urls[last_index_scraped]

            # web scrape the movie
            print ("Movie Url to be scraped: " + movie_url)
            movie = hp.scrape_movie_page(movie_url)

            # print movie data
            hp.print_movie_data(movie)

            if (movie != None):
                # add the movie to the movie list
                movie_list.append(movie)

            # increase index by 1
            last_index_scraped += 1

            # # TODO: Get rid of that two lines (JUST FOR TESTING)
            # if (len(movie_list) == 100):
            #     break

            # do a bulk create everytime 50 movies have been scraped
            if (len(movie_list) == 50):
                ##################################################
                # Add Bulk Documents to Elastic Search Index:
                ################################################## 
                # Add the bulk movie list to the previously created elastic search index
                print ("Add movie list to elasticsearch index '" + ES_INDEX + "'... start")
                es_hp.bulk(es, hp.convert_movie_list_to_bulk(ES_INDEX, movie_list))
                print ("Add movie list to elasticsearch index '" + ES_INDEX + "'... done")

                # store the index of the lastly scraped Url in a file so that on next execution,
                # this function can start from that previous index
                indexFile = open(LAST_INDEX_SCRAPED_TXT, "w") 
                indexFile.write(str(last_index_scraped))

                # close the file
                indexFile.close()

                # reset the movie_list
                movie_list.clear() 
                
        print ("Web Scraping of Url's... done")
        return "Add movie data to Elastic Search succesful"
    except Exception as e:
        errorMsg = "Web Scraping of Url's... FAILED"
        print (errorMsg + ": " + str(e))
        return errorMsg + ": " + str(sys.exc_info()[0])
    
# when the app is launched
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
